// X班 testComplex.c
#include <stdio.h>
#include <math.h>
#include "complex.h"
#include "testCommonWithComplex.h"

// 各自の場所に記載してください。順番が違ってもテストには問題ないので気にしないでください。

/* テスト関数の記述部 */
//##### 33xx XXXX 記述部
void testXXXX() {
    testStart("xxxx");
    assertEqualsInt(xxxx(1), 1);
    assertEqualsInt(xxxx(2), 4);
}
//===== ここまでを 33xx XXXX が記述

//##### 33yy YYYY 記述部
void testYYYY() {
    testStart("yyyy");
    assertEqualsDouble(yyyy(4.0), 2.0);
    assertEqualsDouble(yyyy(9.0), 3.0);
}
//===== ここまでを 33yy YYYY が記述

//##### 33zz ZZZZ 記述部
void testZZZZ() {
    testStart("zzzz");
    assertEqualsDouble(zzzz(1.0, 2.0), sqrt(5.0));
    assertEqualsDouble(zzzz(3.0, 4.0), 5.0);
}
//===== ここまでを 33zz ZZZZ が記述

    
/* main関数の記述部 */
int main(){
    //##### 33xx XXXX 記述部
    testXXXX();
    //===== ここまでを 33zz ZZZZ が記述
    
    //##### 33yy YYYY 記述部
    testYYYY();
    //===== ここまでを 33zz ZZZZ が記述
    
    //##### 33zz ZZZZ 記述部
    testZZZZ();
    //===== ここまでを 33zz ZZZZ が記述
    testErrorCheck(); // これは消さないこと
	return 0;
}

