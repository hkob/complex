// X班 complex.c
#include <stdio.h>
#include <math.h>
#include "complex.h"

// テスト作成者は雛形までを作成し、コンパイルができるまでを責任とします。
// コンパイルができたところで、コミットしてください。

//##### 33xx XXXX 記述部
int xxxx(int x) {
    return x * x;
}

//===== ここまでを 33xx XXXX が記述

//##### 33yy YYYY 記述部
double yyyy(double y) {
    return sqrt(y);
}

//===== ここまでを 33yy YYYY が記述

//##### 33zz ZZZZ 記述部
double zzzz(double a, double b) {
    return sqrt(a * a + b * b);
}
//===== ここまでを 33yy YYYY が記述

