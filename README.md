# X班 複素数関係のライブラリの作成 #

### メンバー (X班) ###

* 33xx XXXX
* 33yy YYYY
* 33zz ZZZZ

### 含まれるもの ###

* complex 関係の関数のためのテスト testComplex.c
* complex 関係のプロトタイプ宣言 complex.h
* complex 関係の関数本体 complex.c
* complex 関数を使った問題を解くプログラム complexMain.c
* complex 関数を使ったグラフを描くプログラム graphMain.c
* 上記をコンパイルするための makefile

### 手順 ###

* make test でテストプログラムを作成し，実行する
* make でサンプルプログラムを作成し，実行する

### リポジトリサービスの利用例の説明文書 ###

* [moodle のブック](http://ee.metro-cit.ac.jp/mdl/mod/book/view.php?id=345)
