// X班 complex.h

#ifndef complex_complex_h
#define complex_complex_h


// complex の定義 by 33aa AAAA
/* ここに構造体の定義を書いてください */
typedef struct {
} complex;

// プロトタイプ宣言部

//##### 33xx XXXX 記述部
int xxxx(int x);

//===== ここまでを 33xx XXXX が記述

//##### 33yy YYYY 記述部
double yyyy(double y);

//===== ここまでを 33yy YYYY が記述

//##### 33zz ZZZZ 記述部
double zzzz(double a, double b);

//===== ここまでを 33yy YYYY が記述

#endif
